#!/bin/bash

url="$1"

baseUrl="https://www.khanacademy.org"

count=1
curl "$url" | hxnormalize -xe | hxselect 'a[data-test-id="unit-header"]' | grep -oP "href=\"/[^>]*" | grep -oP "/[^\"]*" | while read -r line;
do
    position=$(echo $line | awk -F/ '{print length($0) - length($NF)}')
    name=${line:position}
    folderName="$count.$name"
     mkdir $folderName
     cd $folderName
    link="$baseUrl$line"
        echo $link
        curl "$link" | hxnormalize -xe | hxselect '._stw1dyg > a' | grep -oP "href=\"/[^>]*" | grep -oP "/[^\"]*" | while read -r x;
        do
            length=${#x}
            y=${x:0:length-8}
            videoLink="$baseUrl$y"
            youtube-dl $videoLink
        done
    cd ..
    count=`expr $count + 1`
done
