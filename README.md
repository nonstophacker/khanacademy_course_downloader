# khanacademy_course_downloader


**Dependencies**

sudo apt-get install html-xml-utils curl python-pip

sudo pip install youtube-dl

youtube-dl usage example:-

*youtube-dl -o '%(playlist)s/%(playlist_index)s. %(title)s.%(ext)s' https://www.khanacademy.org/math/cc-2nd-grade-math/cc-2nd-add-subtract-100*


**How does it work?**
Given a URL on a specific topic in a course, youtube-dl utility is able to download all embedded youtube videos.

Khan Academy courses are basically a collection of multiple topics. The download script dka.sh basically parses the main course page, extracts the topic URLs 
and iterates through each of them to download embedded videos using youtube-dl utility.


**Usage**
dka.sh <Link to KhanAcademy Course main page>

For example, to download 3rd grade math course from Khan Academy:-

*dka.sh https://www.khanacademy.org/math/cc-third-grade-math*

As of April 29th, 2020, this utility is working as expected. 
 